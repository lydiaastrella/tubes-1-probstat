﻿"TableInfos"
"ID";"Title";"ShortTitle";"Identifier";"Summary";"Modified";"ReasonDelivery";"ExplanatoryText";"Language";"Catalog";"Frequency";"Period";"ShortDescription";"Description";"DefaultPresentation";"DefaultSelection";"GraphTypes";"OutputStatus";"Source";"MetaDataModified";"SearchPriority"
"0";"Emissies van broeikasgassen berekend volgens IPCC-voorschriften";"Emissies van broeikasgassen; IPCC";"70946ned";"Nederlandse emissies van broeikasgassen, berekend volgens de 
IPCC - voorschriften. Naar broncategorie.";"2018-12-18T06:30:00";"Correctie";"De CO2-emissie door de chemische industrie is gecorrigeerd voor de jaren 2012 tot en met 2017. Deze aanpassing is het gevolg van een correctie in de onderliggende energiedata voor enkele grote chemische bedrijven. De opgave van deze bedrijven voor de energiestatistieken van het CBS bleek niet volledig door onduidelijkheid over de afbakening van deze bedrijven. Deze onvolledigheid is ontdekt door een vergelijking van de aan het CBS opgegeven data met de gegevens van deze bedrijven gerapporteerd voor het ETS-systeem en gegevens in de milieujaarverslagen. Een gevolg van deze correctie is dat het finaal verbruik van aardgas nu gemiddeld 22 PJ hoger is voor de jaren 2012 t/m 2017, het olierestgassenverbruik gemiddeld 12 PJ hoger en het elektriciteitsverbruik ongeveer 1 PJ hoger. Deze correcties hebben gevolgen voor berekende emissie van CO2, welke nu gemiddeld 1,3 Mton hoger uitkomen voor de jaren 2012 t/m 2017.";"nl";"CBS";"Perjaar";"1990, 1995, 2000 - 2017";"
Deze tabel bevat cijfers over de totale Nederlandse emissies van de broeikasgassen koolstofdioxide (CO2), lachgas (N2O) en methaan (CH4) door zowel stationaire als mobiele bronnen. Het biedt een inzicht in de Nederlandse emissies van broeikasgassen zoals die worden gerapporteerd van de Verenigde Naties en de Europese Unie. Dit vindt plaats in het kader van de rapportageverplichtingen van het Raamverdrag van de Verenigde Naties inzake klimaatveranderingen (UNFCCC) en van het Bewakingsmechanisme Broeikasgassen van de Europese Unie. De emissies zijn berekend volgens de IPCC-voorschriften. De IPCC (Intergovernmental Panel on Climate Change) verzorgt de wetenschappelijke begeleiding van de uitvoering van het Kyotoprotocol.
Fluorhoudende gassen die bijdragen aan het broeikaseffect zijn niet meegenomen. Hiervoor kan de Emissieregistratie worden geraadpleegd.
De CO2-cijfers zijn inclusief de emissie ten gevolge van het gebruik van smeermiddelen in mobiele bronnen, welke ca. 0,1 miljard kg CO2 omvat.
De cokesfabriek van Tata Steel (Hoogovens)  behoort volgens de CBS Standaard Bedrijfsindeling (SBI) tot de energiesector. In deze tabel zijn de emissies hiervan echter opgenomen bij de Basismetaalindustrie.

Gegevens beschikbaar vanaf: 1990.

Status van de cijfers:
Om een samenhangende en consistente tijdreeks te verkrijgen wordt ieder jaar de complete tijdreeks (her)berekend, zodat de laatste inzichten, met name ten aanzien van de emissiefactoren, in de berekeningen kunnen worden meegenomen. 

Wijzigingen per 18 december 2018:
De CO2-emissie door de chemische industrie is gecorrigeerd voor de jaren 2012 tot en met 2017. Deze aanpassing is het gevolg van een correctie in de onderliggende energiedata voor enkele grote chemische bedrijven. De opgave van deze bedrijven voor de energiestatistieken van het CBS bleek niet volledig door onduidelijkheid over de afbakening van deze bedrijven. Deze onvolledigheid is ontdekt door een vergelijking van de aan het CBS opgegeven data met de gegevens van deze bedrijven gerapporteerd voor het ETS-systeem en gegevens in de milieujaarverslagen. Een gevolg van deze correctie is dat het finaal verbruik van aardgas nu gemiddeld 22 PJ hoger is voor de jaren 2012 t/m 2017, het olierestgassenverbruik gemiddeld 12 PJ hoger en het elektriciteitsverbruik ongeveer 1 PJ hoger. Deze correcties hebben gevolgen voor berekende emissie van CO2, welke nu gemiddeld 1,3 Mton hoger uitkomen voor de jaren 2012 t/m 2017.

Wijzigingen per 10 september 2018:
Toevoeging voorlopige cijfers over 2017.

Wijzigingen per 9 maart 2018:
Stationaire bronnen:
- De nieuwste inzichten, met betrekking tot het gebruik van gasolie/HBO in de dienstensector en de bouwnijverheid, zijn doorgevoerd in de tijdreeks (1990-2016).
- De statistiek Hernieuwbare Energie is gereviseerd; de nieuwe gegevens zijn gebruikt in de emissieberekeningen.

Mobiele bronnen:
- Voorheen heeft het CBS vaste waarden gedurende de gehele tijdreeks gehanteerd voor de verbrandingswaarden van dieselolie en motorbenzine. Deze waarden zijn vóór 1975 geïntroduceerd. Metingen van TNO in 2016 en het RIVM in 2004 toonden aan dat deze verbrandingswaarden, alsmede de koolstofinhoud, aanzienlijk zijn veranderd gedurende de afgelopen decennia. Besloten is de betreffende verbrandingswaarden variabel te maken, afhankelijk van de beschikbare meetwaarden. Op basis van de ter beschikking staande meetwaarden is een tijdreeks van verbrandingswaarden en CO2-emissiefactoren opgesteld. Dit heeft geleid tot wijzigingen in de emissies van CO2 door het gebruik van dieselolie en benzine gedurende de gehele tijdreeks (zie Bronnen en Methoden).
- Er hebben diverse wijzigingen plaatsgevonden in de verbruiksgegevens van brandstoffen, onder andere ten gevolge van de revisie van de Energiestatistieken en aanpassingen in het TNO-rekenmodel voor mobiele werktuigen.
- De CO2-emissies zijn inclusief de indirecte emissies ten gevolge van de emissies van vluchtige organische stoffen door verkeer en vervoer.

Wanneer komen er nieuwe cijfers? 
Definitieve cijfers over 2017 worden in januari 2019 gepubliceerd, voorlopige cijfers over 2018 in september 2019.

";"INHOUDSOPGAVE

1. Toelichting
2. Definities en verklaring van symbolen
3. Koppelingen naar relevante tabellen en artikelen
4. Bronnen en methoden
5. Meer informatie


1. TOELICHTING

Deze tabel bevat cijfers over de totale Nederlandse emissies van de broeikasgassen koolstofdioxide (CO2), lachgas (N2O) en methaan (CH4) door zowel stationaire als mobiele bronnen. Het biedt een inzicht in de Nederlandse emissies van broeikasgassen zoals die worden gerapporteerd van de Verenigde Naties en de Europese Unie. Dit vindt plaats in het kader van de rapportageverplichtingen van het Raamverdrag van de Verenigde Naties inzake klimaatveranderingen (UNFCCC) en van het Bewakingsmechanisme Broeikasgassen van de Europese Unie. De emissies zijn berekend volgens de IPCC-voorschriften. De IPCC (Intergovernmental Panel on Climate Change) verzorgt de wetenschappelijke begeleiding van de uitvoering van het Kyotoprotocol.
Fluorhoudende gassen die bijdragen aan het broeikaseffect zijn niet meegenomen. Hiervoor kan de Emissieregistratie worden geraadpleegd.
De CO2-cijfers zijn inclusief de emissie ten gevolge van het gebruik van smeermiddelen in mobiele bronnen, welke ca. 0,1 miljard kg CO2 omvat.
De cokesfabriek van Tata Steel (Hoogovens)  behoort volgens de CBS Standaard Bedrijfsindeling (SBI) tot de energiesector. In deze tabel zijn de emissies hiervan echter opgenomen bij de Basismetaalindustrie.

Gegevens beschikbaar vanaf: 1990.

Status van de cijfers:
Om een samenhangende en consistente tijdreeks te verkrijgen wordt ieder jaar de complete tijdreeks (her)berekend, zodat de laatste inzichten, met name ten aanzien van de emissiefactoren, in de berekeningen kunnen worden meegenomen. 

Wijzigingen per 18 december 2018:
De CO2-emissie door de chemische industrie is gecorrigeerd voor de jaren 2012 tot en met 2017. Deze aanpassing is het gevolg van een correctie in de onderliggende energiedata voor enkele grote chemische bedrijven. De opgave van deze bedrijven voor de energiestatistieken van het CBS bleek niet volledig door onduidelijkheid over de afbakening van deze bedrijven. Deze onvolledigheid is ontdekt door een vergelijking van de aan het CBS opgegeven data met de gegevens van deze bedrijven gerapporteerd voor het ETS-systeem en gegevens in de milieujaarverslagen. Een gevolg van deze correctie is dat het finaal verbruik van aardgas nu gemiddeld 22 PJ hoger is voor de jaren 2012 t/m 2017, het olierestgassenverbruik gemiddeld 12 PJ hoger en het elektriciteitsverbruik ongeveer 1 PJ hoger. Deze correcties hebben gevolgen voor berekende emissie van CO2, welke nu gemiddeld 1,3 Mton hoger uitkomen voor de jaren 2012 t/m 2017.

Wijzigingen per 10 september 2018:
Toevoeging voorlopige cijfers over 2017.

Wijzigingen per 9 maart 2018:
Stationaire bronnen:
- De nieuwste inzichten, met betrekking tot het gebruik van gasolie/HBO in de dienstensector en de bouwnijverheid, zijn doorgevoerd in de tijdreeks (1990-2016).
- De statistiek Hernieuwbare Energie is gereviseerd; de nieuwe gegevens zijn gebruikt in de emissieberekeningen.

Mobiele bronnen:
- Voorheen heeft het CBS vaste waarden gedurende de gehele tijdreeks gehanteerd voor de verbrandingswaarden van dieselolie en motorbenzine. Deze waarden zijn vóór 1975 geïntroduceerd. Metingen van TNO in 2016 en het RIVM in 2004 toonden aan dat deze verbrandingswaarden, alsmede de koolstofinhoud, aanzienlijk zijn veranderd gedurende de afgelopen decennia. Besloten is de betreffende verbrandingswaarden variabel te maken, afhankelijk van de beschikbare meetwaarden. Op basis van de ter beschikking staande meetwaarden is een tijdreeks van verbrandingswaarden en CO2-emissiefactoren opgesteld. Dit heeft geleid tot wijzigingen in de emissies van CO2 door het gebruik van dieselolie en benzine gedurende de gehele tijdreeks (zie Bronnen en Methoden).
- Er hebben diverse wijzigingen plaatsgevonden in de verbruiksgegevens van brandstoffen, onder andere ten gevolge van de revisie van de Energiestatistieken en aanpassingen in het TNO-rekenmodel voor mobiele werktuigen.
- De CO2-emissies zijn inclusief de indirecte emissies ten gevolge van de emissies van vluchtige organische stoffen door verkeer en vervoer.

Wanneer komen er nieuwe cijfers? 
Definitieve cijfers over 2017 worden in januari 2019 gepubliceerd, voorlopige cijfers over 2018 in september 2019.


2. DEFINITIES EN VERKLARING VAN SYMBOLEN

Definities:
Mobiele bronnen: transportmiddelen en mobiele werktuigen met een verbrandingsmotor. Voorbeelden van transportmiddelen zijn personenauto's, vrachtauto's, binnenvaartschepen en vliegtuigen. Bij mobiele werktuigen moeten we onder andere denken aan landbouwtrekkers, vorkheftrucks en (wegen)bouwmachines.

Stationaire bronnen: vuurhaarden (zoals ovens, kachels en ketels), industriële processen en overige niet-mobiele activiteiten zoals het gebruik van spuitbussen en verf en ontleding van mest (ammoniak).

Emissie: uitstoot, uitworp.

Emissiefactor: emissie per activiteitseenheid, bijvoorbeeld per afgelegde kilometer of per kilogram verbruikte brandstof.

Verklaring van symbolen:

niets (blanco)	: het cijfer kan op logische gronden niet voorkomen
. 		: het cijfer is onbekend, onvoldoende betrouwbaar of geheim
*		: voorlopige cijfers
**		: nader voorlopige cijfers


3. KOPPELINGEN NAAR RELEVANTE TABELLEN EN ARTIKELEN

Relevante tabellen:
Statline biedt een aantal aanverwante tabellen:
-<a href=/""/https://opendata.cbs.nl/statline/#/CBS/nl/dataset/37221/""/>Emissies naar lucht op Nederlands grondgebied; totalen</a> 
-<a href=/""/https://opendata.cbs.nl/statline/#/CBS/nl/dataset/83390NED/""/>Emissies naar lucht op Nederlands grondgebied; stationaire bronnen</a> 
-<a href=/""/https://opendata.cbs.nl/statline/#/CBS/nl/dataset/7062/""/>Emissies naar lucht op Nederlands grondgebied; mobiele bronnen</a> 
-<a href=/""/https://opendata.cbs.nl/statline/#/CBS/nl/dataset/7063/""/>Emissies naar lucht op Nederlands grondgebied; wegverkeer</a> 
-<a href=/""/https://opendata.cbs.nl/statline/#/CBS/nl/dataset/70947NED/""/>Emissies van luchtverontreinigende stoffen volgens NEC-richtlijnen</a>  

De in deze tabel gepubliceerde cijfers over de IPCC-emissies van broeikasgassen wijken op sommige punten af van de berekeningen van de feitelijke emissies op of boven Nederlands grondgebied. 

Verschillen 'feitelijke' en IPCC-emissies:
Volgens de IPCC-voorschriften worden de CO2-emissies door verbranding van biomassa (hout, organisch afval, maar ook biogas) niet meegeteld. De emissies hiervan worden geacht op korte termijn weer te worden opgenomen in planten en bomen. Door hun korte aanwezigheid in de atmosfeer dragen ze niet bij tot een verhoging van de CO2-concentratie. Bij de berekening van feitelijke emissies van 'Emissies naar lucht' op Nederlands grondgebied worden de verbrandingsemissies van biomassa wel meegenomen. 
De feitelijke emissies op Nederlands grondgebied van N2O en CH4 door stationaire bronnen worden op dezelfde wijze als bij de IPCC-methodiek berekend.
Bij de mobiele bronnen bestaat een aantal verschillen, te weten:
- De IPCC-emissies door wegverkeer worden berekend op basis van de afzet van motorbrandstoffen; de feitelijke emissies op basis van het aantal voertuigkilometers op Nederlands grondgebied. De IPCC-emissies van CO2 door wegverkeer zijn gecorrigeerd voor het gebruik van biobrandstoffen. 
- De IPCC-emissies door benzine-recreatievaartuigen zitten in de emissies door wegverkeer, doordat geen onderscheid gemaakt kan worden in de afzet van motorbenzine.
- De IPCC-emissies door vissersschepen worden berekend op basis van de leveringen van dieselolie en zware stookolie aan de visserij; de feitelijke emissies op basis van het aantal vaartuigkilometers binnengaats en op het NCP.
- De zeevaart wordt niet meegenomen bij de berekening van de IPCC-emissies.
- De IPCC-emissies door de binnenvaart omvatten alleen de emissies door schepen met een binnenlandse bestemming. 
- Bij de luchtvaart worden alleen de binnenlandse vluchten meegenomen bij de IPCC-emissies.
- Bij de IPCC-emissies door defensieactiviteiten worden vlieg- en vaarbewegingen voor internationale operaties meegenomen. 

Gegevens over de emissies naar lucht worden ook gepubliceerd in het Compendium voor de Leefomgeving. Dit is een gezamenlijke publicatie van het CBS, het Planbureau voor de Leefomgeving (PBL) en Wageningen University and Research Centre (WUR). Hierin wordt aan de hand van een groot aantal indicatoren informatie verstrekt over milieu, natuur en ruimte: 
<a href=/""/http://www.compendiumvoordeleefomgeving.nl//""/>Compendium voor de leefomgeving</a>.

Gegevens over het gebruik van biobrandstoffen staan in:
<a href=/""/https://opendata.cbs.nl/statline/#/CBS/nl/dataset/71456NED/""/>Biobrandstoffen voor het wegverkeer; aanbod, verbruik en bijmenging.</a> 

Relevante artikelen:
Geen.

Meer informatie is te vinden op de themapagina <a href=/""/https://www.cbs.nl/nl-nl/maatschappij/natuur-en-milieu/""/>Natuur en milieu</a>.


4. BRONNEN EN METHODEN

De jaarlijkse vaststelling door het CBS van de landelijke emissies naar lucht vindt plaats binnen het samenwerkingsverband van de Emissieregistratie. De deelnemende instituten zijn: het CBS, het Rijksinstituut voor Volksgezondheid en Milieu (RIVM), het Planbureau voor de Leefomgeving (PBL), het Landbouw Economisch Instituut LEI), Alterra, Rijkswaterstaat, Deltares, TNO en de Rijksdienst voor Ondernemend Nederland (RVO).
De resultaten van de registratie worden gepubliceerd via de website van de Emissieregistratie:
<a href=/""/http://www.emissieregistratie.nl//""/>www.emissieregistratie.nl</a>.
 
De onderzoeksmethode voor mobiele bronnen is te vinden in de onderzoeksbeschrijving <a href=/""/https://www.cbs.nl/nl-nl/onze-diensten/methoden/onderzoeksomschrijvingen/korte-onderzoeksbeschrijvingen/luchtverontreiniging-emissies-door-mobiele-bronnen /""/>Luchtverontreiniging, emissies door mobiele bronnen</a>.
Daarnaast is in 2011 een artikel verschenen dat naast emissiegegevens ook een uitgebreide samenvatting van de methodiek bevat: <a 
href=/""/https://www.cbs.nl/NR/rdonlyres/4ABCFACE-1251-4136-B5AD-BA861160C5CB/0/2011c175pub.pdf/""/
>Luchtverontreiniging, emissies door mobiele bronnen 1990-2009</a>

De onderzoeksmethode voor stationaire bronnen is te vinden in de onderzoeksbeschrijving <a href=/""/ https://www.cbs.nl/nl-nl/onze-diensten/methoden/onderzoeksomschrijvingen/korte-onderzoeksbeschrijvingen/luchtverontreiniging-emissies-door-stationaire-bronnen /""/>Luchtverontreiniging, emissies door stationaire bronnen</a>.
Daarnaast is in 2011 een artikel verschenen dat naast emissiegegevens ook een uitgebreide samenvatting van de methodiek bevat: <a 
href=/""/https://www.cbs.nl/NR/rdonlyres/98E6F843-1AF0-4EC2-A504-0B1E6CFD0352/0/2011berekeningemissiesstationairebronnenart.pdf/""/
>Berekening van emissies stationaire bronnen door brandstofverbruik</a>.

Methoderapporten voor de vaststelling van de emissies door mobiele (en stationaire) bronnen staan verder op de website van de Emissieregistratie. 
De CBS-cijfers over emissies naar lucht geven de actuele stand van zaken weer. Bij methodewijzigingen wordt, als gevolg van internationale verplichtingen, de complete tijdreeks (her)berekend. De door de Emissieregistratie vastgestelde methodewijzigingen zijn terug te vinden in een uitvoerige internationale rapportage die Nederland elk jaar dient aan te leveren:
<a href=/""/http://unfccc.int/nationalreports/annexighginventories/nationalinventoriessubmissions/items/7383.php/""/>IPCC broeikasgassen</a>.
Deze rapportage omvat ook de oorspronkelijk aangeleverde cijfers, inclusief een duiding van de onzekerheidsmarges.

Een beschrijving van het aanpassen van de verbrandingswaarden en CO2-factoren van dieselolie en motorbenzine staat in een memo op de website van het CBS: <a href=/""/https://www.cbs.nl/en-gb/background/2018/02/adjustment-of-heating-values-and-c02-petrol-and-diesel/""/>Adjustment of heating values and CO2 emission factors of petrol and diesel</a>.


5. MEER INFORMATIE

<a href='https://www.cbs.nl/nl-nl/over-ons/contact/infoservice'>Infoservice</a>

Copyright (c) Centraal Bureau voor de Statistiek, Den Haag/Heerlen 
Verveelvoudiging is toegestaan, mits CBS als bron wordt vermeld.";"ts=1536238991492&graphtype=Table&r=Topics,Perioden&k=Bronnen";"$filter=((Bronnen eq 'T001176  ') or (Bronnen eq 'A025447  ') or (Bronnen eq '800044   ') or (Bronnen eq '800045   ') or (Bronnen eq '1050010  ') or (Bronnen eq '301100   ') or (Bronnen eq 'A025421  ') or (Bronnen eq 'A025424  ') or (Bronnen eq 'A025425  ') or (Bronnen eq 'A025430  ')) and ((Perioden eq '1990JJ00') or (Perioden eq '2000JJ00') or (Perioden eq '2010JJ00') or (Perioden eq '2014JJ00') or (Perioden eq '2015JJ00') or (Perioden eq '2016JJ00') or (Perioden eq '2017JJ00'))&$select=Bronnen, Perioden, CO2_1, CH4_2, N2O_3";"Table,Bar,Line";"Regulier";"CBS.";"2018-12-18T06:30:00";"2"
"DataProperties"
ID;Position;ParentID;Type;Key;Title;Description;ReleasePolicy;Datatype;Unit;Decimals;Default
"0";"0";;"Dimension";"Bronnen";"Bronnen";"";;;;;
"1";"1";;"TimeDimension";"Perioden";"Perioden";"";"true";;;;
"2";"2";;"Topic";"CO2_1";"CO2";"Kooldioxide.

CO2 ontstaat onder andere bij verbranding van de koolstof in brandstoffen. Het is een broeikasgas (veroorzaakt opwarming van de aarde).";;"Long";"mln kg";"0";"Impossible"
"3";"3";;"Topic";"CH4_2";"CH4";"Methaan (= aardgas).

CH4 ontstaat onder andere door onvolledige verbranding van brandstoffen, lekkage van het aardgasnet en door vergisting. Methaan is een broeikasgas (veroorzaakt opwarming van de aarde).";;"Double";"mln kg";"2";"Impossible"
"4";"4";;"Topic";"N2O_3";"N2O";"Distikstofoxide (= lachgas).

N2O ontstaat bij allerlei chemische omzettingsprocessen, met name in de landbouw, door denitrificatieprocessen bij mest en kunstmest en verder onder andere bij de productie van salpeterzuur en in autokatalysatoren. Het is een broeikasgas (veroorzaakt opwarming van de aarde).";;"Double";"mln kg";"2";"Impossible"
"Bronnen"
Key;Title;Description
"T001176  ";"Totaal Stationaire en mobiele bronnen";"Onder de stationaire bronnen vallen onder andere de vuurhaarden (zoals ovens, kachels en ketels), industriële processen en overige niet-mobiele activiteiten zoals het gebruik van spuitbussen en verf en ontleding van mest (ammoniak). Mobiele bronnen zijn transportmiddelen en mobiele werktuigen met een verbrandingsmotor, inclusief buitenlandse transportmiddelen."
"A025447  ";"Stationaire bronnen, totaal";"Onder de stationaire bronnen vallen onder andere de vuurhaarden (zoals ovens, kachels en ketels), industriële processen en overige niet-mobiele activiteiten zoals het gebruik van spuitbussen en verf en ontleding van mest (ammoniak)."
"800044   ";"Energiesector";"Deze categorie is een samentelling van categorieën:
06 Winning van aardolie en aardgas
192 Aardolie-industrie
35 Energiebedrijven"
"305800   ";"06 Winning van aardolie en aardgas";"Winning van aardolie en aardgas"
"320300   ";"192 Aardolie-industrie";"Aardolie-industrie
Deze groep omvat:
Aardolieraffinage:
- vervaardiging van motorbrandstoffen: benzine, kerosine, enz.;
- productie van overige brandstoffen: lichte en zware stookolie, raffinaderijgassen als ethaan, propaan, butaan, enz.;
- vervaardiging van producten voor de petrochemische industrie en voor de vervaardiging van wegdekken;
- vervaardiging van overige aardolieproducten: white spirit, vaseline, paraffine, enz.
Aardolieverwerking (geen -raffinage):
- vervaardiging van smeeroliën en vetten uit aardolie;
- recycling van afgewerkte olie;
- vervaardiging van aardolieproducten uit afvalolie."
"346700   ";"35 Energiebedrijven";"Productie en distributie van en handel in elektriciteit, aardgas, stoom en gekoelde lucht"
"800045   ";"Nijverheid (geen energiesector)";"Deze categorie is een samentelling van categorieën:
B Winning van delfstoffen (geen 06 Winning van aardolie en aardgas)
C Industrie (geen 192 Aardolie-industrie)
F Bouwnijverheid"
"320705   ";"20-21 Chemie en farmaceutische industrie";"Deze categorie is een samentelling van categorieën:
20 Vervaardiging van chemische producten;
21 Vervaardiging van farmaceutische grondstoffen en producten. 
"
"328100   ";"24 Basismetaalindustrie";"Vervaardiging van metalen in primaire vorm. "
"A025441  ";"Overige industrie";"Deze categorie is een samentelling van categorieën:
10-12 Voedings-, genotmiddelenindustrie
13-15 Textiel-, kleding-, lederindustrie
16 Houtindustrie
17-18 Papier- en grafische industrie
29-30 Transportmiddelenindustrie
25-28 Metaalproducten- en machine-industrie
22, 31-33 Overige industrie"
"350000   ";"F Bouwnijverheid";"Bouwnijverheid
Deze sectie omvat:
- algemene en gespecialiseerde bouwkundige en civieltechnische werken, de bouwinstallatie en de afwerking van gebouwen.
Zij omvat ook nieuwbouw, reparatie, aan- en verbouwwerkzaamheden, het optrekken van geprefabriceerde gebouwen of constructies ter plaatse en van tijdelijke bouwwerken.

Onder algemene bouw valt de bouw van woningen, kantoren, winkels en andere vormen van burgerlijke- en utiliteitsbouw enzovoort of de bouw of aanleg van zware constructies als autowegen, straten, bruggen, tunnels, spoorwegen, vliegvelden, havens en andere waterbouwkundige projecten, irrigatiesystemen, rioleringen, industriële installaties, pijpleidingen en elektriciteitsleidingen, sportvoorzieningen enzovoort. Deze werkzaamheden kunnen voor eigen rekening of voor een vast bedrag of op contractbasis worden uitgevoerd. Een deel van de werkzaamheden of soms zelfs alle uitvoerende werkzaamheden kunnen worden uitbesteed aan onderaannemers.

Gespecialiseerde bouw omvat de bouw of aanleg van een gedeelte van bouwwerken en van civieltechnische werken of de hiervoor vereiste voorbereidende werkzaamheden. Er is gewoonlijk sprake van gespecialiseerde werkzaamheden ten behoeve van diverse bouwwerken, waarvoor specifieke ervaring of een speciale uitrusting nodig is. Het heien, leggen van funderingen, boren van waterputten, de cascobouw, het storten van beton, metselen, zetten van natuursteen, de bouw van steigers, dakbedekking enzovoort worden hiertoe gerekend. Gespecialiseerde bouwkundige werkzaamheden worden meestal aan onderaannemers uitbesteed, maar vooral reparaties worden in de bouw rechtstreeks voor de eigenaar van het onroerend goed uitgevoerd.

De bouwinstallatie omvat de installatie van alle voorzieningen waardoor een bouwwerk als zodanig zijn functie kan vervullen. Deze werkzaamheden worden meestal op de bouwplaats zelf verricht, hoewel bepaalde gedeelten ervan in een werkplaats kunnen worden uitgevoerd. Inbegrepen zijn loodgieterswerk, de installatie van verwarmings- en klimaatregelingssystemen, alarmsystemen en andere elektrische apparatuur, sprinklerinstallaties, liften en roltrappen enzovoort. Ook vallen hieronder isolatiewerkzaamheden (vochtwering, warmte- en geluidsisolatie), het aanbrengen van metalen beplating, de installatie van commerciële koelapparatuur, verlichtings- en signaleringssystemen voor wegen, spoorwegen, luchthavens, havens enzovoort. Het uitvoeren van reparaties in verband met deze activiteiten is ook inbegrepen.

De afwerking van gebouwen omvat werkzaamheden die zijn gericht op de afwerking of voltooiing van een bouwwerk, zoals glaszetten, stukadoorswerk, schilderen, sauzen, het aanbrengen van vloer- of wandtegels of van andere bekleding of bedekking zoals parket, tapijt, behang enzovoort, schuren van vloeren, aftimmeren, geluidstechnische werkzaamheden enzovoort. Het uitvoeren van reparaties in verband met deze activiteiten is ook inbegrepen.

Deze sectie omvat niet:
- de bouw of installatie van industriële apparatuur en machines (bijvoorbeeld de installatie van industriële ovens, turbines enzovoort)
(sectie C);
- het optrekken van volledige geprefabriceerde gebouwen of bouwwerken van zelf vervaardigde onderdelen wordt ingedeeld bij de toepasselijke rubriek van de sectie Industrie, afhankelijk van het materiaal waaruit deze voornamelijk bestaan. Indien dit echter beton is, wordt deze activiteit in deze afdeling ingedeeld."
"1050010  ";"Particulier huishouden";"Eén of meer personen die samen een woonruimte bewonen en zichzelf, dus niet-bedrijfsmatig, voorzien in de dagelijkse levensbehoeften."
"800051   ";"A,E,G-U Overige sectoren (stationair)";"Deze categorie is een samentelling van categorieën:
A Landbouw, bosbouw en visserij
E Waterbedrijven en afvalbeheer
G-U Dienstverlening"
"301100   ";"01 Landbouw (stationaire bronnen)";"Landbouw, jacht en dienstverlening voor de landbouw en jacht"
"A025446  ";"Diensten, afval en water";"E Waterleidingbedrijven en afvalbeheer. 
G-U Commerciële en niet-commerciële dienstverlening.
"
"A025421  ";"Mobiele bronnen; totaal";"Mobiele bronnen zijn transportmiddelen en mobiele werktuigen, exclusief zeevaart, internationale binnenvaart en internationale luchtvaart, inclusief defensie-activiteiten. Berekeningen op basis van het energieverbruik volgens de Nederlandse Energiehuishouding (CBS-NEH).
"
"A025422  ";"Vervoer";"Alle vervoer van personen en goederen over rail, weg, water en door de lucht, exclusief zeevaart, internationale binnenvaart, internationale luchtvaart en vervoer op het eigen bedrijfsterrein.
"
"A025423  ";"Railverkeer";"Alle vervoer van personen en goederen door de dieseltractie van de spoorwegen. Vervoer op het eigen bedrijfsterrein valt hier niet onder.
"
"A025424  ";"Wegverkeer";"Alle vervoer van personen en goederen via de openbare weg. Vervoer op het eigen bedrijfsterrein valt hier niet onder. Berekeningen op basis van het energieverbruik volgens de Nederlandse Energiehuishouding (/""/fuel sold/""/).
"
"A025425  ";"Scheepvaart";"Alle vervoer van personen en goederen via waterwegen, inclusief recreatievaart, exclusief zeevaart en internationale binnenvaart. Visserij valt hier niet onder. Emissie-berekeningen op basis van het energieverbruik volgens de Nederlandse Energiehuishouding (CBS-NEH).
"
"A025428  ";"Luchtvaart";"Alle vervoer van personen en goederen via de lucht. Het betreft hier alleen binnenlandse vluchten. Berekeningen op basis van het energieverbruik van de Nederlandse Energiehuishouding (CBS-NEH).
"
"A025429  ";"Overige mobiele bronnen, totaal";"Visserij (vissersschepen) en werktuigen met een verbrandingsmotor; hieronder vallen bijvoorbeeld landbouwtrekkers, vorkheftrucks, kranen en bouwmachines. 
Visserij-emissies berekend op basis van brandstofleveringen (NEH).
"
"A025430  ";"Landbouw (mobiele bronnen)";"Deze post bevat mobiele werktuigen, voornamelijk landbouwtrekkers, inclusief van loonwerkers en verhuurbedrijven.
"
"A025431  ";"Visserij";"Emissies berekend op basis van brandstofleveringen volgens de Nederlandse Energiehuishouding (CBS-NEH).
"
"A025432  ";"Defensie-activiteiten";"Marineschepen en militaire vliegtuigen, ook voor internationale operaties. De emissies zijn berekend op basis van de brandstofleveringen volgens de Nederlandse Energiehuishouding.
"
"A025433  ";"Overige sectoren (mobiele bronnen)";"Werktuigen met een verbrandingsmotor buiten de landbouwsector. Hieronder vallen bijvoorbeeld vorkheftrucks, kranen en bouwmachines.

"
"Perioden"
Key;Title;Description;Status
"1990JJ00";"1990";;"Definitief"
"1995JJ00";"1995";;"Definitief"
"2000JJ00";"2000";;"Definitief"
"2001JJ00";"2001";;"Definitief"
"2002JJ00";"2002";;"Definitief"
"2003JJ00";"2003";;"Definitief"
"2004JJ00";"2004";;"Definitief"
"2005JJ00";"2005";;"Definitief"
"2006JJ00";"2006";;"Definitief"
"2007JJ00";"2007";;"Definitief"
"2008JJ00";"2008";;"Definitief"
"2009JJ00";"2009";;"Definitief"
"2010JJ00";"2010";;"Definitief"
"2011JJ00";"2011";"";"Definitief"
"2012JJ00";"2012";"";"Definitief"
"2013JJ00";"2013";;"Definitief"
"2014JJ00";"2014";;"Definitief"
"2015JJ00";"2015";"";"Definitief"
"2016JJ00";"2016";"Definitieve cijfers";"Definitief"
"2017JJ00";"2017";"Voorlopige cijfers";"Voorlopig"
